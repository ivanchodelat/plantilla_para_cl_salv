*&---------------------------------------------------------------------*
*&  Include           Z_SALV_TOP
*&---------------------------------------------------------------------*

tables: zmxhirmm001.


TYPES: BEGIN OF tt_out,
         "!Idoc Numero
         docnum TYPE edi_docnum,
         "!Número de documento material
         mblnr TYPE mblnr,
         "!Ejercicio del documento de material
         mjahr TYPE mjahr,
         "!Número de documento de un documento de factura
         belnr TYPE re_belnr,
         "!Ejercicio
         gjahr TYPE gjahr,
         "!Texto de mensaje para la interfase de diálogo CAD
         messg TYPE camsg,
         "!Nombre de usuario
         uname TYPE uname,
         "!Fecha
         datum TYPE datum,
         "!Hora
         uzeit TYPE uzeit,
       END OF tt_out.
