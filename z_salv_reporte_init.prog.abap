*&---------------------------------------------------------------------*
*& Include zppr_002_reporte_init
*&---------------------------------------------------------------------*

DATA obj_sel        TYPE REF TO lcl_selection.          "Objeto para datos de seleccion
DATA obj_data       TYPE REF TO lcl_data.               "Objeto para busqueda de datos
DATA obj_alv        TYPE REF TO lcl_alv.                "Objetos para mostrar el ALV
INITIALIZATION.
  CREATE OBJECT obj_sel.
  CREATE OBJECT obj_data EXPORTING iobj_sel = obj_sel.
  CREATE OBJECT obj_alv EXPORTING iobj_data = obj_data. "Objeto que muestra el ALV
  IF  obj_sel IS BOUND.
    obj_alv->obj_sel = obj_sel.
  ENDIF.


START-OF-SELECTION.

*Loading Selection Parameters in the Selection Parameters Object

  obj_sel->p_pspnr = p_pspnr.

 obj_sel->t_prart_range[] = s_prart[].
 obj_sel->t_pspnr_range[] = s_pspnr[].
 obj_sel->t_tplnr_range[] = s_tplnr[].
 obj_sel->t_vornr_range[] = s_vornr[].
