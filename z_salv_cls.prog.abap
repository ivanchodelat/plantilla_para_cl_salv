*&---------------------------------------------------------------------*
*& Include zppr_002_cls
*&---------------------------------------------------------------------*

CLASS lcl_selection DEFINITION.
**********************************************************************
  PUBLIC SECTION.
*..RANGOS de Selección
   TYPES tt_prart_range  TYPE RANGE OF prps-prart.
    "!Clase del proyecto
   DATA  t_prart_range   TYPE tt_prart_range.

   TYPES tt_tplnr_range  TYPE RANGE OF prps-tplnr.
   "!Ubicación técnica
   DATA  t_tplnr_range   TYPE tt_tplnr_range.

   TYPES tt_pspnr_range  TYPE RANGE OF prps-pspnr.
   "!Elemento PEP
   DATA  t_pspnr_range   TYPE tt_pspnr_range.


   TYPES tt_vornr_range  TYPE RANGE OF afvc-vornr.
   "!Número de operación
   DATA  t_vornr_range   TYPE tt_vornr_range.

*..Parámetros
    DATA:
     "!Proyecto (interno)
     p_pspnr  TYPE proj-pspnr.

ENDCLASS.

**********************************************************************
CLASS lcl_data DEFINITION.
**********************************************************************
  PUBLIC SECTION.
    METHODS constructor
      IMPORTING iobj_sel TYPE REF TO lcl_selection.
    METHODS  search_data
      RAISING cx_qie_no_number_range
      .
    METHODS raise_exc_if_data_empty
      IMPORTING p_tabla TYPE ANY TABLE
      RAISING   cx_list_error_empty_list.
  PRIVATE SECTION.
    DATA:
      obj_sel          TYPE REF TO lcl_selection.

ENDCLASS. "-------------DEFINITION------------------------lcl_data
CLASS lcl_data IMPLEMENTATION.
  METHOD constructor.
*  IMPORTING iobj_sel TYPE REF TO lcl_selection.
    me->obj_sel = iobj_sel.

  ENDMETHOD.                    "constructor

  METHOD search_data.

  ENDMETHOD.

  METHOD raise_exc_if_data_empty.
*     IMPORTING VALUE(p_tabla) TYPE ANY TABLE
*      RAISING   cx_list_error_empty_list.
    DESCRIBE TABLE p_tabla LINES DATA(lv_num_bins).
    IF lv_num_bins EQ 0.
      RAISE EXCEPTION TYPE cx_list_error_empty_list.
    ENDIF.
  ENDMETHOD.

ENDCLASS."----------------------lcl_data------------


*&---------------------------------------------------------------------*
*& Clase para manejar los eventos
*&---------------------------------------------------------------------*
CLASS lcl_event_handler DEFINITION FINAL.
  PUBLIC SECTION.
    METHODS : on_double_click FOR EVENT double_click OF cl_salv_events_table
      IMPORTING row column.
    METHODS : on_link_click FOR EVENT link_click OF cl_salv_events_table
      IMPORTING row column.
    METHODS: constructor IMPORTING  igr_data TYPE REF TO lcl_data.
    DATA: lr_data TYPE REF TO lcl_data.
  PRIVATE SECTION.


ENDCLASS.

*----------------------------------------------------------------------*
*       CLASS lcl_handle_events IMPLEMENTATION
*----------------------------------------------------------------------*
CLASS lcl_event_handler IMPLEMENTATION.

  METHOD constructor.
    lr_data = igr_data.

  ENDMETHOD.


  METHOD on_double_click.
    DATA idoc  TYPE docnum.
    DATA bdcdata_tab TYPE TABLE OF bdcdata.
    DATA opt TYPE ctu_params.


  ENDMETHOD.                    "on_double_click
  METHOD on_link_click.
    MESSAGE i001(00) WITH 'Click on link: column- ' column ' / line- ' row.
  ENDMETHOD.                    "on_link_click








ENDCLASS.                    "lcl_handle_events IMPLEMENTATION



*&---------------------------------------------------------------------*
*& Include          ZMXHIRMM001_LC_ALV
*&---------------------------------------------------------------------*
* Esta clase se encarga de mostrar el ALV                             +
*---------------------------------------------------------------------+

CLASS lcl_alv DEFINITION.
  PUBLIC SECTION.

    METHODS constructor
      IMPORTING iobj_data TYPE REF TO lcl_data.

    METHODS generate_alv.

    DATA obj_data TYPE REF TO lcl_data.
    DATA obj_salv TYPE REF TO cl_salv_table.
    DATA obj_sel  TYPE REF TO lcl_selection.
    DATA obj_events     TYPE REF TO cl_salv_events_table.   "Eventos
    DATA obj_handler    TYPE REF TO lcl_event_handler.      "Manejador de eventos

    DATA t_functions           TYPE REF TO cl_salv_functions.
    DATA obj_dspset            TYPE REF TO cl_salv_display_settings.
    DATA lr_column             TYPE REF TO cl_salv_column_table.
    DATA lv_salv_columns_table TYPE REF TO cl_salv_columns_table.
    DATA lo_columns            TYPE REF TO cl_salv_columns_table.

  PRIVATE SECTION.
    DATA: lo_column TYPE REF TO cl_salv_column.

    METHODS set_color.
    METHODS encabezado_alv.
    METHODS set_col_names.
    METHODS set_columnname
      IMPORTING
        i_sh_tit            TYPE scrtext_s OPTIONAL
        VALUE(i_md_tit)     TYPE scrtext_m OPTIONAL
        VALUE(i_lg_tit)     TYPE scrtext_l OPTIONAL
        VALUE(i_nombre_act) TYPE lvc_fname.
    METHODS get_total_registros
      RETURNING VALUE(r_result) TYPE i.

ENDCLASS.                    "lcl_alv DEFINITION

*----------------------------------------------------------------------*
*       CLASS lcl_alv IMPLEMENTATION
*----------------------------------------------------------------------*
*
*----------------------------------------------------------------------*
CLASS lcl_alv IMPLEMENTATION.
  METHOD constructor.
*  IMPORTING iobj_data TYPE REF TO lcl_data.
    me->obj_data = iobj_data.
  ENDMETHOD.                    "constructor

  METHOD set_color.
* Set color to a particular row based on your condition.

  ENDMETHOD.                    "set_color

  METHOD encabezado_alv.
*
    DATA: lo_header  TYPE REF TO cl_salv_form_layout_grid,
          lo_h_label TYPE REF TO cl_salv_form_label,
          lo_h_flow  TYPE REF TO cl_salv_form_layout_flow.
*
*   header object
    CREATE OBJECT lo_header.
*
*   To create a Lable or Flow we have to specify the target
*     row and column number where we need to set up the output
*     text.
*
*   information in Bold

********************
* Macro de header para el titulos
    DEFINE  _macro_set_titulo.
      lo_h_label = lo_header->create_label( row = &1 column = &2 ).
      lo_h_label->set_text(  &3 ).
    END-OF-DEFINITION.


    _macro_set_titulo:
*   ROW     COL     Texto,                               "contenido
    1       1       'Reporte Idoc´s Aplicados.',         "Titulo:
    2       3       'Parámetros',
    2       4       'de Selcción'.

* Macro para los datos de el header
* viene de  dos etiquea: dato
    DEFINE  _macro_set_labels.
      lo_h_flow = lo_header->create_flow( row = &1  column = &2 ).
      lo_h_flow->create_text( text = &3 )." Fecha ejecución
*
      lo_h_flow = lo_header->create_flow( row = &1  column = &2 + 1 ).
      lo_h_flow->create_text( text = &4 ).
    END-OF-DEFINITION.

    DATA(lv_total) = me->get_total_registros( ).

    _macro_set_labels:
*   ROW     COL     Texto1                      TEXTO2       "contenido texto1
    3       1       'Fecha ejecución:'          sy-datum    ,"Fecha ejecución
    4       1       'Usuario:'                  sy-uname    ,"Usuario:
    5       1       'Total de Registros:'       lv_total    ."Total de Registros


* Criterios de seleccion
    IF  me->obj_sel IS BOUND.
* Macro para los criterios de selección
      DEFINE _macro_set_lab_criter_sel.
        lo_h_flow = lo_header->create_flow( row = &1  column = &2 ).
        lo_h_flow->create_text( text = me->obj_sel->get_desc_data_element( &3 ) ).
        lo_h_flow = lo_header->create_flow( row = &1  column = &2 + 1  ).
        lo_h_flow->create_text( text = me->obj_sel->&4 ).
      END-OF-DEFINITION.

*      obj_sel->cargar_extremos_datum( ).
*
*      _macro_set_lab_criter_sel:
**   ROW     COL    Data_elemnt   Paramet_name
*    3       4      'DATUM'       dat1,
*    4       4      'DATUM'       dat2.


    ENDIF.
*
*   set the top of list using the header for Online.
    me->obj_salv->set_top_of_list( lo_header ).
*
*   set the top of list using the header for Print.
    me->obj_salv->set_top_of_list_print( lo_header ).
*
  ENDMETHOD.                    "encabezado_alv

  METHOD generate_alv.

    TRY.
        CALL METHOD cl_salv_table=>factory
          IMPORTING
            r_salv_table = me->obj_salv
          CHANGING
            t_table      = gt_alv_out.
      CATCH cx_salv_msg into data(cx_salv_msg).
    ENDTRY.


*... Events--------------------------------------
    " Set event handling
    obj_events = obj_salv->get_event( ).

    " Create handler instance
    CREATE OBJECT obj_handler
      EXPORTING
        igr_data = obj_data.

    " Set event handler
    SET HANDLER obj_handler->on_double_click FOR obj_events.
    SET HANDLER obj_handler->on_link_click FOR obj_events.

*-------------------------------------------------
*//  Get List of columns
    lo_columns = me->obj_salv->get_columns( ).
    lo_columns->set_optimize( 'X' ).

    me->set_col_names( ).

*// Layouts
    DATA gs_key TYPE salv_s_layout_key.
    gs_key-report = sy-repid.
    obj_salv->get_layout( )->set_key( gs_key ).
    obj_salv->get_layout( )->set_save_restriction( ).
    obj_salv->get_layout( )->set_default( abap_true ).


*//  Set the Color Column to the ALV
    TRY.
        lo_columns->set_color_column(  'COLOR' ).
      CATCH cx_salv_data_error.
    ENDTRY.

    me->t_functions = me->obj_salv->get_functions( ).
    me->t_functions->set_all( abap_true ).

    me->set_color( ).
    me->encabezado_alv( ).

    me->obj_salv->display( ).

  ENDMETHOD.                    "generate_alv


  METHOD set_col_names.
    DATA: nombre_act TYPE lvc_fname,
          titulo     TYPE scrtext_s.
**********************************************************************
* Macro para llenar los titulos y hacer muy legible el código
    DEFINE _macro_nombre_col.
      set_columnname(
            i_&1_tit     = &3
            i_nombre_act = &2 ).
    END-OF-DEFINITION.

*    _macro_nombre_col sh:  "Nombre corto
** Dominio          Titulo         ,"Columna Especificación Funcional
**-+-----------------+---------------,----------------
*     'DOCNUM'       ''              ,"
*     'MBLNR'        ''              ,"
*     'MJAHR'        ''              ,"
*     'BELNR'        ''              ,"
*     'GJAHR'        ''              ,"
*     'MESSG'        ''              ,"
*     'UNAME'        ''              ,"
*     'DATUM'        ''              ,"
*     'UZEIT'        ''              ."
*
*    _macro_nombre_col md:
*
*    _macro_nombre_col lg:


  ENDMETHOD." ---------------set_col_names-----------------

  METHOD set_columnname.
*      IMPORTING
*        i_sh_tit     TYPE scrtext_s OPTIONAL
*        i_md_tit     TYPE scrtext_s OPTIONAL
*        i_lg_tit     TYPE scrtext_s OPTIONAL
*        i_nombre_act TYPE lvc_fname.
*---------------------------------------
    TRANSLATE i_nombre_act TO UPPER CASE.
    IF  i_md_tit IS  INITIAL.
      i_md_tit = i_sh_tit.
    ENDIF.
    IF i_lg_tit IS INITIAL.
      i_lg_tit = i_sh_tit.
    ENDIF.

    TRY.
        IF  i_sh_tit IS NOT INITIAL.
          lo_columns->get_column( i_nombre_act )->set_short_text( i_sh_tit ).
        ENDIF.
        IF  i_md_tit IS NOT INITIAL.
          lo_columns->get_column( i_nombre_act )->set_medium_text( i_md_tit ).
        ENDIF.
        IF i_lg_tit IS NOT INITIAL.
          lo_columns->get_column( i_nombre_act )->set_long_text( i_lg_tit ).
        ENDIF.
      CATCH cx_salv_not_found.                          "#EC NO_HANDLER

    ENDTRY.

  ENDMETHOD.


  METHOD get_total_registros.
* RETURNING  VALUE(r_result) TYPE i.
    DESCRIBE TABLE gt_alv_out LINES r_result.
  ENDMETHOD.



ENDCLASS.




"! Clase para funciones generales
CLASS lcl_funciones DEFINITION.
**********************************************************************
  PUBLIC SECTION.

    CLASS-METHODS raise_if_fail_auth
      IMPORTING VALUE(i_burks) TYPE bukrs
      RAISING   cx_authorization_missing.

ENDCLASS."-------------DEFINITION------------------------lcl_funciones
**********************************************************************
CLASS lcl_funciones IMPLEMENTATION.

  METHOD raise_if_fail_auth.
*  IMPORTING VALUE(i_burks) TYPE bukrs
*              RAISING cx_authorization_missing.
    DATA: lv_msg(100).
    AUTHORITY-CHECK OBJECT 'F_BKPF_BUK'
                ID 'BUKRS' FIELD i_burks
                ID 'ACTVT' FIELD '03'.
    IF sy-subrc <> 0.
      RAISE EXCEPTION TYPE cx_authorization_missing.
    ENDIF.

  ENDMETHOD.
ENDCLASS. " ---------------lcl_funciones IMPLEMENTATION.
