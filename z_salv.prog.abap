*&-----------------------------------------------------------------------------------------*
* Report              : ZPPR_002                                                           *
* Autor               : Iván Castillo de la Torre   I4 solcuciones                         *
* Fecha Creación      : Mayo 24 2019                                                    *
* Consult. Funcional  :                                            *
*------------------------------------------------------------------------------------------*
* Título              : Ordenes de Producción de concreto                        *
* Descripción         :    *
*                          *
*                        *
*                       .                                             *
*------------------------------------------------------------------------------------------*
* Modulo              : PP                                                                 *
* Transac Asociada    :                                                               *
*------------------------------------------------------------------------------------------*
*  GESTION DE VERSIONES                                                                    *
*------------------------------------------------------------------------------------------*
*                                           *
*------------------------------------------------------------------------------------------*
*  DIAGRAMA DE FLUJO DE DATOS                                                              *
*------------------------------------------------------------------------------------------*
*         *
*------------------------------------------------------------------------------------------*
INCLUDE zppr_002_top.               "Global definitions
INCLUDE zppr_002_sel_screen.        "Selection Screen
INCLUDE zppr_002_cls.               "Classes
INCLUDE zppr_002_reporte_init.      "initialization and star of selection
*  .. Main()
TRY.
    "Buscar la información
    obj_data->search_data( ).
    "Verificar que exista información
    obj_data->raise_exc_if_data_empty( gt_alv_out ).
    "Verficar el objeto de autorización
"    lcl_funciones=>raise_if_fail_auth( obj_sel->p_bukrs ).
    "Mostrar el ALV
    obj_alv->generate_alv( ).

    "-----------------Exceptions --------------------------------
*..Ahotirizacion fallida
  CATCH cx_authorization_missing INTO DATA(cx_aut).
    MESSAGE 'Usted no tiene autorización.'(001)  TYPE 'E'.
*.. No hay datos pra los parametros selecionados
  CATCH cx_list_error_empty_list INTO DATA(cx_list).
    MESSAGE s002(wusl) DISPLAY LIKE 'E'.


*.. algo inesperado paso
  CATCH cx_root INTO DATA(gcx_root).
    MESSAGE i499(sy) WITH gcx_root->get_text( ) DISPLAY LIKE 'E'.
ENDTRY.

END-OF-SELECTION.